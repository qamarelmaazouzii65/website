---
title: Managing Infrastructure as Code on Alibaba Cloud vs Microsoft Azure
date: "2020-01-11T00:00:00.000Z"
description: "A quick comparison of Alibaba Cloud Resource Orchestration Service and Microsoft Azure Resource Manager for Infrastructure as Code usage."
published: true
tags: [ "microsoft azure", "alibaba cloud", "infrastructure as code", "azure resource manager", "devops" ]
---

## Managing Infrastructure as Code on Alibaba Cloud vs Microsoft Azure

With the advent of DevOps, many organizations have started migrating their
infrastructure management to a more secure and reversible format of storage.
Thus, the advent of the term, “Infrastructure-as-Code”. This term, sometimes
misunderstood, is a very fancy way of saying, that just the way we version
controlled our source codes, we are now going to version our infrastructures.
The growth to this keyword was provided with the growth of cloud computing and
the as-a-service and pay-as-you-go deployment models.

### Infrastructure-as-Code: Overview

First consider an organization with their infrastructure deployed on-premises.
This requires that organization performs a complete study of their requirements
and the potential growth they are expecting to accommodate the hardware and
infrastructure costs in the coming years. In case they require to upgrade
their infrastructure due to some elements that were not foreseen—due to any
reason, user increase (or decrease), software patch requirements, etc.—they
would need to redeploy almost everything. In classical methods, this would
follow up with a waterfall approach to patch everything. This could lead to
a huge capital investment requirement and would lead to slow production releases
due to compliance delays.

With cloud, this has changed. Cloud vendors takes care of all these elements for
us, and with DevOps they have introduced several tools to manage the infrastructure
(mentioned as IaC from now on in the blog). IaC tools allow operational teams to
operate at scale while,

1. Leaving the infrastructure deployment to these tools.
1. Managing configurations for VMs and compute engines.
1. Supporting declarative syntax for resource creation and management.
1. Being version controllable as any other code, in text format.

These properties for IaC tools enable operations teams to manage their infrastructure,
and **rollback to an earlier stage** in case their infrastructure does not perform
well, or the budget does not support the updated infrastructure demands.
In this post, I will discuss two industry leading options for IaC management,
one is Microsoft Azure’s ARM Template and other one is Alibaba Cloud’s Resource
Orchestration Service, or ROS for short.

## ARM Template by Microsoft Azure

ARM Template is one of the most used solution on Microsoft Azure. It is powered
by the Azure Resource Manager, which enables resource grouping and location parameters
to deploy the solutions on the cloud. Moreover, templates are also used to define
the infrastructure and the resources deployed.

ARM Template takes a couple of important details to “manage” an infrastructure,

1. Location where to deploy
1. Resources
    * Their quantity and their types
1. Connections between resources
    * Think of this as the virtual networks they are being deployed in
1. Extra information

Infrastructure is deployed on Azure using the information provided in this template
by Azure schedulers and controllers. A template can be downloaded for each resource
that is deployed—whether a single resource or a group of resources. In each resource,
look for “**Export Template**” and utilize the JSON document that is provided. You
can also use Azure SDKs to generate the template or consume the template.

![Azure ARM Template showing the schema for a resource.](azure-arm-template.png)

DevOps tools utilize this template to deploy or re-deploy the infrastructure in
case of need. This also leads to deployment of multiple stages and environments—
testing, QA, staging, production—which can help different departments and different
teams to utilize the platform as per their necessities.

Security of user data as well as infrastructure configuration is taken important
care of as most details are requested on-demand during deployment process. Other
Azure services such as Azure Key Vault service can be utilized to provide the
credentials and other sensitive details for the deployment of infrastructure and
configuration.

You can study the basic [template structure here](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-authoring-templates),
or explore a few options of [Azure ARM template here](https://azure.microsoft.com/en-us/resources/templates/).
In a nutshell, let us recap the pros and cons of every platform:

### Pros of Azure ARM Templates

1. Native support for management of Azure resources.
1. Contains conditional scripts to accept input and dropdown selections.
1. Automatically configures and hides sensitive input from users.
1. Comes free of cost, and templates can be stored for later purposes.

### Cons of Azure ARM Templates

1. Can only be used with Azure resources.
1. No capability to contain automation jobs, only state.
1. No possibility to sketch or generate diagrams of the infrastructure.

## Alibaba Cloud Resource Orchestration Service

On Alibaba Cloud, we are provided with Resource Orchestration Service (referred
to as ROS from here on) that helps us manage the infrastructure, as code, while
staying on top of security and operational ease. Alibaba Cloud ROS follows
similar patterns as other IaC tools and is provided as a first-class and free
service for the customers.

On Alibaba Cloud, ROS follows a stack-deployment model—which on Microsoft Azure is
known as a resource group. Stack Deployment is the deployment of a template that
has been generated by Alibaba Cloud ROS. Sample templates can be used to quickly
deploy and test the Alibaba Cloud infrastructure, or for testing and learning purposes.

![Alibaba Cloud ROS template being created in the portal](alibaba-cloud-ros-sample.png)

The most beautiful feature of Alibaba Cloud is the capability of visually representing
the infrastructure. Alibaba Cloud ROS can be utilized to draw architectural diagrams
that support business needs. Many consultancy organizations rely on this type of
feature for their cloud platforms that they are presenting a marketing pitch for.
Thus, portal can convert your JSON document to a visual (editable) graph, that you
can edit and modify as per your needs.

![Alibaba Cloud ROS showing the infrastructure as a diagram](alibaba-cloud-ros-visuals.png)

This template can be deployed to Alibaba Cloud or saved for later uses. Saving
the template in local environment can help operational teams version this infrastructure
template using a version control system; such as git.
Alibaba Cloud SDK can be used to download the templates stored in a subscription
for complete automation. In case infrastructure does not perform well, a complete
stack can be deployed again using the same template. Templates are text-based;
thus, they can be used to rollback to a specific infrastructure state as per
requirements of customers or resources. Now let’s enlist the pros and cons of
Alibaba Cloud Resource Orchestration Service, pros first.

### Pros of Alibaba Cloud Resource Orchestration Service

1. Native integration with Alibaba Cloud platform and resources.
1. Available free of cost for every account.
1. Enables resource provisioning through status queries.
1. Uses text-based structure to define the infrastructure states and configurations.
1. Out-of-box capabilities to draw a graph of infrastructure for visualization.

### Cons of Alibaba Cloud Resource Orchestration Service

1. Available only for Alibaba Cloud platform and resources.
1. Does not support automation scripts.

## Operational Ease

Both solutions support features necessary for a product to be considered an
Infrastructure-as-Code tool. Azure ARM Templates are strictly tied to Resource
Groups and they create resource groups upon deployments. Resource groups enable
grouping and collective upgradation of services, in case of a down time. ARM
Templates do not have a visual editor—officially supported by Azure, but they
are capable of declarative expression of an infrastructure.

Alibaba Cloud ROS on the other hand provides all the features as an IaC tool and
is a separate orchestrator service—not depending on a first-class product. ROS
also has a visual editor that helps in editing the infrastructure configurations
as well as in presenting the infrastructure to customers and potential clients
for sales or marketing pitch.

Deployment of resources depends entirely on the infrastructure and both these
products are alike in this aspect.
