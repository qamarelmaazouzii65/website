---
title: "Software Developer's 2019 Review and New Year Resolution"
description: "Since everybody is doing it, let me give it a try too! My new year resolutions based on my mistakes and wrong turns in 2019, 2018 and more years."
date: "2020-01-01T00:00:00.000Z"
author: "Afzaal Ahmad Zeeshan"
tags: [ "happy new year", "software developer", "fifa", "alibaba cloud" ]
published: true
---

I am about to be a decade old in software development, and open source communities.
I have had the honor to collaborate with some of the finest in the industry, and
write articles, and do courses with a few of the best in the market. But, there are
some things that not everyone says in the market. But, those who know me, know that
I enjoy saying what I feel any time of the day.

2019 was not an amazing year for me, and I do not hold any grudges against 2019 for
that, I was a bit lazy throughout the year, and I missed a lot of interesting
opportunities because of my laziness. I was also busy in a couple of important things;
family-related.

## Learning

Over the previous year as well as 2018, I explored several languages, tools and
sometimes I went out of my field and studied some history, science and photography
concepts. I tried to post some of the work that I had done online on my community
profiles but as fate had it, I was a lot busy so it was not sure what I was doing.
Most of the communities and organizations (especially Microsoft) had to remove my
from their MVP Award lists due to lack of online activities.

Here are the top 5 topics I want to learn in 2020:

1. .NET Core (I have little depth-knowledge of framework)
1. Data structures, algorithms (I would read research papers on these topics)
1. JavaScript as a language (React as a front-end framework)
1. Mathematics of Data Science and Machine Learning
1. Quantum Physics (for Quantum Computations)

Overall my 2020 would be spend rock-soliding the concepts I already know. It is
my bad habit of leaving the framework in the middle and exploring something new.

## Family and friends

I feel (believe, and I know) that for a software engineer, friends and their family
members are always avoided, no matter what. I tried to spend maximum time with
my friends and family. I tried to check my friends every month (if not every two
weeks) and trust me, it has been amazing spending time with them each time.

I decided to go on a foreign trip with my family. I was with my mother and brother,
and we decided to go to Malaysia to meet relatives. Although, it was hardly a 10
day trip, it was a trip to cherish.

Here is me and my brother (Daniyal Ahmad Rizwan) at Batu Caves temple:

![Afzaal Ahmad Zeeshan and Daniyal Ahmad Rizwan at Batu Caves temple, Malaysia](malaysia-batu-saves.jpg)

I am planning our next trip, but before that, I need a good contract to pay
my monthly bills. :sigh:

To our friends and families: We do not do that intentionally. It is a part of our
firmware.

## Community and Open Source

I have explored a bunch of new software and communities that are used by open source
communities. GitLab has been on the top of my list. Fortunately, I was able to use
GitLab for several of my own projects as well as recommend it to my friends. The
software has a great potential for future. You can check out my [GitLab profile](https://gitlab.com/afzaal-ahmad-zeeshan)
and maybe collaborate on a few of the open source boilerplate code that I am
working on.

![GitLab account of Afzaal Ahmad Zeeshan](gitlab-account-afzaal-ahmad-zeeshan.png)

Alibaba Cloud also has a great set of open source projects on their cloud platform
as well as their eCommerce platforms. Check out the [project catalouge](https://www.alibabacloud.com/blog/12-open-source-projects-by-alibaba--part-1_582638)
on their website.

I am currently focused on Kubernetes, Cloud-Native and Mobile development as of 2020
. I have explored a bunch of frameworks and runtimes and I feel like this much
of exploration has only made my grip on .NET (and .NET Core) a lot weaker. I read
many articles about C#, .NET Core, Xamarin and other of my all time favorite
languages and frameworks, and I feel so outdated and out of touch with them. So,
I plan to work more on .NET Core, ASP.NET Core, and SQL Server &mdash; going
old school!

> I am currently working on a book for **DevSecOps enthusiasts for .NET Core**, keep
> an eye out and I will post as soon as the book becomes available.
>
> I plan to write about the challenges that DevSecOps solves and the tools that
> you can use to perform automated security, performance and compliance checks
> on your software packages.

If you are working on an open source project, maybe we can collaborate! :smiley:

## Games and Entertainment

I spent 2019 playing games and enjoying movies, songs and travelling across country
as well as outside Pakistan. I will add my travel experience about Qatar and Malaysia
in their own respective posts, but for the time being, let us just enlist the moments
that I enjoyed.

Here is my hand (my watch, and my FIFA band) in Khalifa International Stadium, Doha,
Qatar, for the FIFA Clubs World Cup 2019.

![My hand with the FIFA band on it, with a watch in Khalifa International Stadium.](my-watch-fifa-clubs-world-cup-2019.jpg)

Here I am in the lobby, looking for food:

![Afzaal Ahmad Zeeshan looking for food at Khalifa International Stadium during
final match.](afzaal-ahmad-zeeshan-at-fifa-clubs-world-cup-2019.jpg)

If I would like to present my 2019 in a nutshell, it would be this:

![My 2019 in a nutshell!](fifa-clubs-world-cup-2019-logo.jpg)

## New hobby still not found

I am still looking for a new hobby. I have tried:

1. Technical and non-technical writing.
2. Sports.
3. Travel and adventure.
4. Sleeping throughout the day.

None of the activities have so far helped me in any way. I have worked with
some publishers in the past, and authoring was not that much interesting. Plus,
the place where I live in extra-noisy. There is a lot of traffic, and construction
going on that prevents me from doing a video course (on time). Packt is a highly
recommended publisher in my opinion, everyone should collaborate with them, at
least once.

Right now, I am planning on studies, do my MS, then PhD and then get a job. If
nothing works, I might just join a company as a software engineer.
