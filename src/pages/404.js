import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

class NotFoundPage extends React.Component {
  render() {
    // const { data } = this.props
    const siteTitle = this.props.data.site.siteMetadata.title

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="Lost" />
        <h1>Oops...</h1>
        <p>This page doesn't exist, did you take a wrong turn somewhere? If the page is broken, I will fix it... Or add a new one as needed!</p>
        <p>You may visit the <Link to="/">home page</Link>.</p>
      </Layout>
    )
  }
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
