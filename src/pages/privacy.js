import React from "react"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"

class PrivacyPolicyPage extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="Homepage" description="This website contains blogs, articles, tip/tricks and other content that I have worked on." />
        <Bio />
        <h4>Privacy Policy</h4>
        <p>This page applies to all of the applications (unless mentioned otherwise on the application about page) that I maintain, write and own. The privacy policy describes how I access the user information (if required), how is it used (on the user's machine or web servers) and what is shared with me (as a person, outside the web servers) or public audience.</p>
        <h4>What is accessed from your device</h4>
        <p>If any of my applications accesses your data, you are notified prior for about such intents in description for that application before you download and install it. At most only your device information is stored to know where you log in from. My systems are not designed to track your automatically identity, such as (but not limited to) IP addresses, unless you grant permission.</p>
        <p>These are accessed and are kept offline in the application as long as you use or when you chose to sync on OneDrive. Otherwise no data is accessed or used from your device. Most of my applications store data offline on your machine which can be wiped easily from your machine. You should check your device's method of clearing cache data.</p>

        <p><b>Note:</b> Your personal account information is never used, tampered or shared with public audience and I (or the company) would never send spam emails for any promotional work, unless you share your consent to subscribe to a mailing list.</p>
        <h4>How is it used?</h4>
        <p>The information collected from your device is used in your own device. Like, for example, to build a better UX and UI for the application. We may use your name or information to fill in the views for our application. None of this information is shared with any of third-party partners or organizations.</p>
        <p>I integrate monitoring and analytics tools and software to track the usage of my products. The tracking is entirely anonymous with default values, and no identification is stored and the data collected is not used to identify the users.</p>
        <p>We may use the API services in your device to provide you services and data on your device, online store servers (such as for Push Notifications), because we will be using native APIs. With the requests, no personal data will be shared and requests will be anonymous (unless allowed to contain name/contact detail information) for specialized response. No personal user data will be shared with any third-party service, if needed to share the data you will be notified prior to installation or during the set up.</p>
        <h4>What is shared with others</h4>
        <p>I (or the company that I represent and will represent in future) are not interested in your data and we respect your privacy and data. Our applications are just to serve you and facilitate you in your processes and management and never to share your data maliciously with others to earn anything.</p>
        <h4>Ads and other partners</h4>
        <p>I might include some advertisement SDKs in my application to earn some revenue. Their caching and cookie policies are their own, and I will add links to those where necessary. I do not control their data collection metrics, you should consult their privacy policies to learn how they use data.</p>
        <h4>Changes and updates</h4>
        <p>Any changes or updates in our privacy policy will be shared on this page and this page will always be updated to make sure that you are aware of our actions and you know how your data is being used to serve you. If you ever want to opt-out you can remove our application or cancel any subscriptions that you have.</p>
      </Layout>
    )
  }
}

export default PrivacyPolicyPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
