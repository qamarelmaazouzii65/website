import React from "react"
import { Link, graphql } from "gatsby"
import _ from 'lodash';
import { GoTag } from 'react-icons/go';

import Bio from "../components/bio"
// import Tags from "../components/tags"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm, scale } from "../utils/typography"

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext
    const description = post.frontmatter.description || post.excerpt;

    let tableOfContents = "";
    if(post.tableOfContents && post.tableOfContents.length > 0) {
      tableOfContents = <div><h4>Post Agenda</h4><div dangerouslySetInnerHTML={{__html: post.tableOfContents}}></div><hr/></div>;
    }

    let tags = "";
    if(post.frontmatter.tags && post.frontmatter.tags.length > 0) {
      tags = post.frontmatter.tags.map(tag => {
        return <Link
                  style={
                    {
                      margin: rhythm(0.4),
                      cursor: "hand",
                      padding: rhythm(0.4),
                      boxShadow: "none",
                      border: "1px solid #007acc"
                    }
                  }
                  to={`/tags/${_.kebabCase(tag)}`}><GoTag /> {tag}</Link>
      });
    }

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.frontmatter.title}
          description={description}
        />
        <article>
          <header>
            <h1
              style={{
                marginTop: rhythm(1),
                marginBottom: 0,
              }}
            >
              {post.frontmatter.title}
            </h1>
            <p
              style={{
                ...scale(-1 / 5),
                display: `block`,
                marginBottom: rhythm(1),
              }}
            >
              {post.frontmatter.date}
            </p>
            <blockquote style={{fontSize: rhythm(0.5)}}>{description}</blockquote>

          </header>
          <div>
            {tableOfContents}
          </div>
          <section dangerouslySetInnerHTML={{ __html: post.html }} />
          <hr
            style={{
              marginBottom: rhythm(1),
            }}
          />
          <footer>
            <div>
              {tags}
            </div><br />
            <Bio />
          </footer>
        </article>

        <nav>
          <ul
            style={{
              display: `flex`,
              flexWrap: `wrap`,
              justifyContent: `space-between`,
              listStyle: `none`,
              padding: 0,
            }}
          >
            <li>
              {previous && (
                <Link to={previous.fields.slug} rel="prev">
                  ← {previous.frontmatter.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link to={next.fields.slug} rel="next">
                  {next.frontmatter.title} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      tableOfContents
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        tags
      }
    }
  }
`
