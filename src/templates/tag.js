import React from "react"
// import PropTypes from "prop-types"
import { GoHome } from 'react-icons/go'

// Components
// import Layout from "../components/layout"

// Components
import { Link, graphql } from "gatsby"

const TagComponent = ({ pageContext, data }) => {
  const { tag } = pageContext
  const { edges, totalCount } = data.allMarkdownRemark
  const tagHeader = `${totalCount} post${
    totalCount === 1 ? "" : "s"
  } tagged with "${tag}"`

  return (
    <div style={{textAlign: "center", maxWidth: "950px", margin: "0 auto"}}>
      <h1>{tagHeader}</h1>
      <div style={{textAlign: "left"}}>
        <ul>
          {edges.map(({ node }) => {
            const { slug } = node.fields
            const { title } = node.frontmatter
            return (
              <li key={slug}>
                <Link to={slug}>{title}</Link>
              </li>
            )
          })}
        </ul>
      </div>
      {/*
              This links to a page that does not yet exist.
              You'll come back to it!
            */}
      {/* <p>You can also explore <Link to="/tags">all tags &rarr;</Link></p> */}
      <p><GoHome /> <Link to="/">Homepage</Link></p>
    </div>
  )
}

// Prop verification, not necessary right now.
// TagComponent.propTypes = {
//   pageContext: PropTypes.shape({
//     tag: PropTypes.string.isRequired,
//   }),
//   data: PropTypes.shape({
//     allMarkdownRemark: PropTypes.shape({
//       totalCount: PropTypes.number.isRequired,
//       edges: PropTypes.arrayOf(
//         PropTypes.shape({
//           node: PropTypes.shape({
//             frontmatter: PropTypes.shape({
//               title: PropTypes.string.isRequired,
//             }),
//             fields: PropTypes.shape({
//               slug: PropTypes.string.isRequired,
//             }),
//           }),
//         }).isRequired
//       ),
//     }),
//   }),
// }

export default TagComponent

export const pageQuery = graphql`
  query($tag: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`
