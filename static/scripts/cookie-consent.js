(function (document) {
  function consentGiven() {
    console.log('consent given on button..');
    localStorage.setItem("cookie-consent-check", true);
  }

  let consentHtml = `
  <p>
    This website uses cookie. Cookies are placed on your machine to keep a track of the activities on this website. The information is not used to identify you, but just to keep a track of how application is being used.
  </p>
  <button class="btn btn-default" onclick="consentGiven();">Ok.</button>
  `;

  if(window.localStorage) {
    let cookieConsent = localStorage.getItem("cookie-consent-check");
    if(!cookieConsent) {
      // Show the content.
      console.log('creating element...');
      let consentDiv = document.createElement("div");
      consentDiv.innerHTML(consentHtml);
      document.body.appendChild(consentDiv);
      console.log('added the element.');
    } else {
      console.log('consent given.');
    }
  } else {
    console.log("Cookie consent cannot be stored in the browser.");
  }
  console.log("injected.")
})(document);
